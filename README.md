# Hi!
To see the docs for downloading books from `safaribooks.com`, click [here](https://github.com/lorenzodifuccia/safaribooks)

## Rule: Private Repository. Only to be shared with those you completely trust, and if you will share it to them, only give them `read` level.

If you have epubs you would also like to share, just branch out, put it inside proper category, and PR.

This is thanks to the safari book downloader (link above).

In order to facilitate batch downloads, `safariRunner.py` was created to read a list of safari book IDs and download them 1 by 1.
Sample txt list:
```
9781260128734
9781317595700
9781351144308
9781351650274
9781351206099
9781351713290
```

To use `safariRunner.py`, open the file
```
#! python3

import os

books = []
with open ('books.txt') as f:
    books = f.read().splitlines()

for book in books:
    os.system('python3 safaribooks.py --cred "<email>:<password>" ' + book)
```
Create a 10 day trial safari account, then replace <email> and <password> in 

```os.system('python3 safaribooks.py --cred "<email>:<password>" ' + book)```

with your own safaribooks email and password. Run the python file with the command below

```python3 safariRunner.py```


![a](/rejoice.jpg)